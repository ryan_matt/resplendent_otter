# Responsible for maintaining ActiveRecord Report model
class Report < ActiveRecord::Base
  belongs_to :author, class_name: 'User'
  belongs_to :referee

  delegate :display_name, to: :author, prefix: true
  delegate :display_name, to: :referee, prefix: true

  validates :rules_knowledge_score, :right_of_way_score, :strip_control_score,
            numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 10 }
end
