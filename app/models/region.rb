# Responsible for the ActiveRecord region model
class Region < ActiveRecord::Base
  has_many :referees
  has_many :users
end
