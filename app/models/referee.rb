# Responsible for maintaining the ActiveRecord referee model
class Referee < ActiveRecord::Base
  has_many :reports
  belongs_to :region

  validates :first_name, :last_name, :region_id, :email, presence: true
  validates :epee_rating,
            :foil_rating,
            :sabre_rating,
            numericality: { greater_than_or_equal_to: 1, less_than_or_equal_to: 10 }

  delegate :name, to: :region, prefix: true

  def display_name
    "#{first_name} #{last_name}"
  end

  def search_name
    "#{last_name} #{first_name}"
  end
end
