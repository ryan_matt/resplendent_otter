# responsible for maintaining the ActiveRecord User model
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  validates :first_name, :last_name, :region_id, presence: true

  belongs_to :region
  has_many :referees
  delegate :name, to: :region, prefix: true

  def display_name
    "#{first_name} #{last_name}"
  end

  def search_name
    "#{last_name} #{first_name}"
  end
end
