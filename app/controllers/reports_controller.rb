# Responsible for routing reports HTTP requests
class ReportsController < ApplicationController
  def index
    render locals: { reports: Report.where(author: current_user) }
  end

  def show
    render locals: { report: Report.find(params[:id]) }
  end

  def new
    render 'edit',
           locals: { report: Report.new, action_name: 'New', submit_path: reports_path }
  end

  def create
    report = Report.new(params[:report].permit(permitted_params))
    report.author = current_user

    if report.save
      redirect_to report
    else
      render 'edit', locals: { report: report, action_name: 'New', submit_path: reports_path }
    end
  end

  private

  def permitted_params
    [
      :referee_id,
      :rules_knowledge_score,
      :rules_knowledge_comments,
      :right_of_way_score,
      :right_of_way_comments,
      :strip_control_score,
      :strip_control_comments
    ]
  end
end
