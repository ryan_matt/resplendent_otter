# Responsible for routing /referees/ requests
class RefereesController < ApplicationController
  def index
    render locals: { referees: Referee.all.order('last_name asc') }
  end

  def new
    render 'edit',
           locals: { referee: Referee.new, action_title: 'New', submit_path: referees_path }
  end

  def create
    referee = Referee.new(params[:referee].permit(permitted_params))
    if referee.save
      redirect_to referee
    else
      render 'edit'
    end
  end

  def show
    render locals: { referee: Referee.find(params[:id]) }
  end

  def edit
    ref = Referee.find(params[:id])
    render locals: { referee: ref, action_title: 'Edit', submit_path: referee_path(ref) }
  end

  def update
    referee = Referee.find(params[:id])
    if referee.update(params[:referee].permit(permitted_params))
      redirect_to referee
    else
      render 'edit', locals: { referee: referee,
                               action_title: 'Edit',
                               submit_path: referee_path(referee) }
    end
  end

  private

  def permitted_params
    [
      :email,
      :first_name,
      :last_name,
      :region_id,
      :epee_rating,
      :foil_rating,
      :sabre_rating
    ]
  end
end
