Rails.application.routes.draw do
  devise_for :users
  root 'dashboard#index'
  resources :referees
  resources :reports
end
