echo 'Running RSpec'
bundle exec rspec
if [ "$?" -eq 0 ]
        then
                echo 'Running RuboCop'
                bundle exec rubocop --rails --auto-correct
                echo 'Running Haml Lint'
                bundle exec haml-lint app/views/
                echo 'Running RubyCritic'
                bundle exec rubycritic app/ lib/
fi
