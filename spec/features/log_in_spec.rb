require 'rails_helper'

describe 'logging in' do
  it 'renders a log in page' do
    visit new_user_session_path
    expect(page).to have_content 'Email'
    expect(page).to have_content 'Password'
  end

  context 'with a registered user' do
    let(:user) { create :user }

    it 'lets a user log in' do
      visit new_user_session_path
      fill_in 'Email', with: user.email
      fill_in 'Password', with: user.password
      click_button 'Log in'
      expect(current_path).to eq root_path
    end

    it 'fails a user with the wrong password' do
      visit new_user_session_path
      fill_in 'Email', with: user.email
      fill_in 'Password', with: 'notpassword'
      click_button 'Log in'
      expect(current_path).to eq new_user_session_path
      expect(page).to have_content 'Invalid email or password'
    end
  end
end
