require 'rails_helper'

describe 'registering a new user' do
  let!(:region) { Region.create name: 'Test Region' }

  it 'renders a new user page' do
    visit new_user_registration_path
    expect(page).to have_content 'Email'
    expect(page).to have_content 'Password'
    expect(page).to have_content 'Password confirmation'
  end

  it 'registers a new user' do
    visit new_user_registration_path
    fill_in 'Email', with: 'test@email.com'
    fill_in 'Password', with: 'password'
    fill_in 'Password confirmation', with: 'password'
    fill_in 'First name', with: 'Test F Name'
    fill_in 'Last name', with: 'Test L Name'
    select region.name
    click_button 'Sign up'
    user = User.last
    expect(current_path).to eq root_path
    expect(user.email).to eq 'test@email.com'
    expect(user.encrypted_password).to_not be_empty
    expect(user.sign_in_count).to eq 1
  end

  it 'prevents signing up with a bad password confirmation' do
    visit new_user_registration_path
    fill_in 'Email', with: 'test1@email.com'
    fill_in 'Password', with: 'password'
    fill_in 'Password confirmation', with: 'notpassword'
    fill_in 'First name', with: 'Test F Name'
    fill_in 'Last name', with: 'Test L Name'
    select region.name
    click_button 'Sign up'
    expect(page).to have_content '1 error prohibited this user from being saved'
    expect(page).to have_content 'Password confirmation doesn\'t match Password'
  end

  it 'prevents signing up without a name' do
    visit new_user_registration_path
    fill_in 'Email', with: 'test1@email.com'
    fill_in 'Password', with: 'password'
    fill_in 'Password confirmation', with: 'password'
    select region.name
    click_button 'Sign up'
    expect(page).to have_content '2 errors prohibited this user from being saved'
    expect(page).to have_content "First name can't be blank"
    expect(page).to have_content "Last name can't be blank"
  end

  it 'prevents signing up with a bad password confirmation' do
    visit new_user_registration_path
    fill_in 'Email', with: 'test1@email.com'
    fill_in 'Password', with: 'password'
    fill_in 'Password confirmation', with: 'password'
    fill_in 'First name', with: 'Test F Name'
    fill_in 'Last name', with: 'Test L Name'
    click_button 'Sign up'
    expect(page).to have_content '1 error prohibited this user from being saved'
    expect(page).to have_content "Region can't be blank"
  end
end
