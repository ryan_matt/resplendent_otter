FactoryGirl.define do
  factory :user do
    first_name 'Test first name'
    last_name 'Test last name'
    sequence(:email) { |n| "email#{n}@test.com" }
    password 'test_password'
    password_confirmation 'test_password'
    region
  end
end
