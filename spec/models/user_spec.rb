require 'rails_helper'

RSpec.describe User, type: :model do
  let(:region) { build :region, name: 'test region' }
  let(:user) { build :user, region: region }

  specify { user.region_name == 'test region' }
  specify { user.display_name == "#{user.first_name} #{user.last_name}" }
  specify { user.search_name == "#{user.last_name}, #{user.first_name}" }
  pending 'add new User specs'
end
