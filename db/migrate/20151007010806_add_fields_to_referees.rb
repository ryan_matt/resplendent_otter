class AddFieldsToReferees < ActiveRecord::Migration
  def change
    add_column :referees, :phone_number, :string
  end
end
