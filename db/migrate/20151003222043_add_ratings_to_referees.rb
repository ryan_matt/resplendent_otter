class AddRatingsToReferees < ActiveRecord::Migration
  def change
    add_column :referees, :epee_rating, :integer
    add_column :referees, :foil_rating, :integer
    add_column :referees, :sabre_rating, :integer
  end
end
