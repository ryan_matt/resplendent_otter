class CreateRefereesAndReports < ActiveRecord::Migration
  def change
    create_table :referees do |t|
      t.integer :region_id, null: false
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.string :email, null: false
      t.string :secondary_email

      t.timestamps
    end
    add_foreign_key :referees, :regions

    create_table :reports do |t|
      t.integer :user_id, null: false
      t.integer :referee_id, null: false
      t.integer :rules_knowledge_score
      t.text :rules_knowledge_comments
      t.integer :right_of_way_score
      t.text :right_of_way_comments
      t.integer :strip_control_score
      t.text :strip_control_comments

      t.timestamps null: false
    end
    add_foreign_key :reports, :users
    add_foreign_key :reports, :referees
  end
end
