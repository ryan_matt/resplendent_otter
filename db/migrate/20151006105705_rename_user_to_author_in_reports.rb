class RenameUserToAuthorInReports < ActiveRecord::Migration
  def change
    rename_column :reports, :user_id, :author_id
  end
end
