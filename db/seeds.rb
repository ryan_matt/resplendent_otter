['North Atlantic',
 'Mid-Atlantic',
 'Midwest',
 'Southwest',
 'Mountain',
 'Pacific Coast',
 'Northwest',
 'Southeast'].each do |str|
  Region.create(name: str)
end

User.create(first_name: 'Test',
            last_name: 'User',
            email: 'test@email.com',
            password: 'password',
            region: Region.last)

5.times do |n|
  Referee.create(first_name: "Ref#{n}",
                 last_name: "Name#{n}",
                 email: "email#{n}@test.com",
                 region: Region.find(n % Region.count + 1),
                 epee_rating: n,
                 foil_rating: n,
                 sabre_rating: n)
end

3.times do |n|
  Report.create(author: User.first,
                referee: Referee.find(n % Referee.count + 1),
                rules_knowledge_score: 1,
                rules_knowledge_comments: 'rk comment',
                right_of_way_score: 2, 
                right_of_way_comments: 'row comment',
                strip_control_score: 3,
                strip_control_comments: 'sc comment')
end
